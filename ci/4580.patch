From e2873bc614e50d19f1b8aa939514c83933b10136 Mon Sep 17 00:00:00 2001
From: Eli Schwartz <eschwartz@archlinux.org>
Date: Thu, 10 Mar 2022 00:08:36 -0500
Subject: [PATCH 1/2] meson: use proper handling of wayland-protocols
 dependency

Ensure that resolution of the subproject occurs via the dependency
interface, not the "poke at subprojects manually" interface, and make
that actually work via --wrap-mode=forcefallback.

There's no need to mark it as not-required and then manually invoke
subproject(), since fallback should work correctly and it is always
needed.

However, if fallback was performed (or forced) it would error out since
get_variable() was instructed to only use pkg-config while the relevant
variable was exported by the subproject as an internal fallback
dependency.
---
 meson.build | 8 ++------
 1 file changed, 2 insertions(+), 6 deletions(-)

diff --git a/meson.build b/meson.build
index 63b4d73bfe..17fdb1cfe7 100644
--- a/meson.build
+++ b/meson.build
@@ -517,15 +517,11 @@ if wayland_enabled
   wlclientdep    = dependency('wayland-client', version:  wayland_req,
                               fallback: 'wayland',
                               default_options: ['documentation=false'])
-  wlprotocolsdep = dependency('wayland-protocols', version: wayland_proto_req, required: false)
+  wlprotocolsdep = dependency('wayland-protocols', version: wayland_proto_req)
   wlegldep       = dependency('wayland-egl',
                               fallback: 'wayland')
 
-  if not wlprotocolsdep.found()
-    wlproto_dir = subproject('wayland-protocols').get_variable('wayland_protocols_srcdir')
-  else
-    wlproto_dir = wlprotocolsdep.get_variable(pkgconfig: 'pkgdatadir')
-  endif
+  wlproto_dir = wlprotocolsdep.get_variable('pkgdatadir')
 
   wayland_pkgs = [
     'wayland-client @0@'.format(wayland_req),
-- 
GitLab


From dc875b6af9311f3b5275961618ad867bf1f57a1e Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Wed, 16 Mar 2022 09:53:16 +0200
Subject: [PATCH 2/2] meson: switch some .wrap files to stable branches

To avoid random failures if one of the projects starts
depending on new things or has incompatible changes.
---
 subprojects/glib.wrap              | 2 +-
 subprojects/graphene.wrap          | 2 +-
 subprojects/libepoxy.wrap          | 2 +-
 subprojects/sysprof.wrap           | 2 +-
 subprojects/wayland-protocols.wrap | 2 +-
 subprojects/wayland.wrap           | 2 +-
 6 files changed, 6 insertions(+), 6 deletions(-)

diff --git a/subprojects/glib.wrap b/subprojects/glib.wrap
index c785910d9f..ec5febde86 100644
--- a/subprojects/glib.wrap
+++ b/subprojects/glib.wrap
@@ -2,5 +2,5 @@
 directory=glib
 url=https://gitlab.gnome.org/GNOME/glib.git
 push-url=ssh://git@gitlab.gnome.org:GNOME/glib.git
-revision=main
+revision=glib-2-68
 depth=1
diff --git a/subprojects/graphene.wrap b/subprojects/graphene.wrap
index 902095dbd7..d9503ab978 100644
--- a/subprojects/graphene.wrap
+++ b/subprojects/graphene.wrap
@@ -1,5 +1,5 @@
 [wrap-git]
 directory=graphene
 url=https://github.com/ebassi/graphene.git
-revision=master
+revision=1.10.6
 depth=1
diff --git a/subprojects/libepoxy.wrap b/subprojects/libepoxy.wrap
index fc5147a22b..0ab256ef26 100644
--- a/subprojects/libepoxy.wrap
+++ b/subprojects/libepoxy.wrap
@@ -1,5 +1,5 @@
 [wrap-git]
 directory=libepoxy
 url=https://github.com/anholt/libepoxy.git
-revision=master
+revision=1.5.9
 depth=1
diff --git a/subprojects/sysprof.wrap b/subprojects/sysprof.wrap
index 99aa36ce87..41fb7c6ee2 100644
--- a/subprojects/sysprof.wrap
+++ b/subprojects/sysprof.wrap
@@ -1,5 +1,5 @@
 [wrap-git]
 directory=sysprof
 url=https://gitlab.gnome.org/GNOME/sysprof.git
-revision=master
+revision=sysprof-3-38
 depth=1
diff --git a/subprojects/wayland-protocols.wrap b/subprojects/wayland-protocols.wrap
index 065651db63..5361f0c540 100644
--- a/subprojects/wayland-protocols.wrap
+++ b/subprojects/wayland-protocols.wrap
@@ -1,5 +1,5 @@
 [wrap-git]
 directory=wayland-protocols
 url=https://gitlab.freedesktop.org/wayland/wayland-protocols.git
-revision=main
+revision=1.23
 depth=1
diff --git a/subprojects/wayland.wrap b/subprojects/wayland.wrap
index 5be67bf491..bdb12d3aa7 100644
--- a/subprojects/wayland.wrap
+++ b/subprojects/wayland.wrap
@@ -1,7 +1,7 @@
 [wrap-git]
 directory=wayland
 url=https://gitlab.freedesktop.org/wayland/wayland.git
-revision=main
+revision=1.20.0
 depth=1
 
 [provide]
-- 
GitLab

